namespace CacheProvider.AppFabric
{
    using System;
    using Microsoft.ApplicationServer.Caching;

    internal class DataCacheFactoryEntry
    {
        public DataCacheFactory DataCacheFactory { get; private set; }

        public Guid FactoryId { get; private set; }

        public DataCacheFactoryEntry(DataCacheFactory dataCacheFactory)
        {
            this.DataCacheFactory = dataCacheFactory;
            this.FactoryId = Guid.NewGuid();
        }
    }
}