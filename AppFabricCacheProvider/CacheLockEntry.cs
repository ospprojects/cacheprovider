namespace CacheProvider.AppFabric
{
    #region N A M E S P A C E   I M P O R T S

    using System;
    using Microsoft.ApplicationServer.Caching;

    #endregion

    [Serializable]
    internal class CacheLockEntry
    {

        public DataCacheLockHandle LockHandle { get; private set; }
        public Guid LockId { get; private set; }
        public CacheLockEntry(DataCacheLockHandle lockHandle)
        {
            this.LockHandle = lockHandle;
            this.LockId = Guid.NewGuid();
        }
    }
}
