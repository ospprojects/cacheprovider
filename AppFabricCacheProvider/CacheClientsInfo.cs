namespace CacheProvider.AppFabric
{
    #region N A M E S P A C E   I M P O R T S

    using System.Collections.Generic;
    using Microsoft.ApplicationServer.Caching;

    #endregion

    public class CacheClientsInfo
    {
        public List<AppFabricCacheClientInfo> AppFabricCacheClients { get; set; }
        public DataCacheItem DataCacheItem { get; set; }

        public CacheClientsInfo()
        {
            AppFabricCacheClients = new List<AppFabricCacheClientInfo>();
        }
    }
}
