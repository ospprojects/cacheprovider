﻿namespace CacheProvider.AppFabric
{
    #region N A M E S P A C E   I M P O R T S

    using System;

    #endregion

    /// <summary>
    /// Holds information about AppFabric Cache Client
    /// </summary>
    [Serializable]
    public class AppFabricCacheClientInfo
    {
        public string ApplicationName { get; set; }
        public string ClientIdentifier { get; set; }
        public bool IsClientAlive { get; set; }
        public string MACAddress { get; set; }
        public string IPAddress { get; set; }
    }
}
