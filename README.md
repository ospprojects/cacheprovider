# CacheProvider For .NET# 

The main objective of this project is to enable provider based caching API to be used in .Net based application.

#Current release:# [v4.0](https://bitbucket.org/ospprojects/cacheprovider/downloads/ProviderBasedCache%20v4.0.zip)

To clone this project, use the following git command

```
$ git clone https://hariharanpalani@bitbucket.org/ospprojects/cacheprovider.git
```

Please refer [wiki](https://bitbucket.org/ospprojects/cacheprovider/wiki/Home) for more documentation.