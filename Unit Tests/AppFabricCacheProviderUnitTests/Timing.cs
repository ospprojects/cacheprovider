﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;

namespace AppFabricCacheProviderUnitTests
{
    public class Timing
    {
        private TimeSpan _startTime;
        private TimeSpan _duration;
        public Timing()
        {
            _startTime = TimeSpan.Zero;
            _duration = TimeSpan.Zero;
        }

        public void Start()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            _startTime = Process.GetCurrentProcess().Threads[0].UserProcessorTime;
           
        }

        public void Stop()
        {
            _duration = Process.GetCurrentProcess().Threads[0].UserProcessorTime.Subtract(_startTime);
        }

        public TimeSpan ElapsedTime
        {
            get
            {
                return _duration;
            }
        }
    }
}
