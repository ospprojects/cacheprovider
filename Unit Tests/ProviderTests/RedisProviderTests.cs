﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CacheProvider.Redis;
using System.Collections.Specialized;
using System.Threading;
using System.Diagnostics;

namespace ProviderUnitTests
{
    /// <summary>
    /// Summary description for RedisProviderTests
    /// </summary>
    [TestClass]
    public class RedisProviderTests
    {
        private TestContext testContextInstance;
        private RedisCacheProvider _provider;
        private bool _callBackCalled;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        [TestInitialize()]
        public void TestInitialize()
        {
            _callBackCalled = false;
            _provider = new RedisCacheProvider();
            var configuration = new NameValueCollection();
            configuration.Add("defaultExpirationTimeInMS", "5000");
            _provider.Initialize("RedisCacheProvider", configuration);
            _provider.Remove("Name");
            _provider.Remove("Employees");
            _provider.Remove("NameInRegion", "Test");
        }
        //
        // Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup()
        {
            _provider = null;
        }
        //
        #endregion

        [TestMethod]
        public void BasicAddTests()
        {
            var key = "Name";
            var value = "RedisCacheProvider Add";

            var actual = _provider.Add(key, value);

            Assert.IsTrue(actual);

            var expected = _provider.Get(key);
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void AddWithTypeTest()
        {
            var key = "Employees";
            var emp = new Employee
            {
                Name = "Employee 1"
            };

            var actual = _provider.Add(key, emp);

            Assert.IsTrue(actual);

            var expected = _provider.Get(key);

            Assert.AreEqual(expected, emp);
        }

        [TestMethod]
        public void AddWithDefaultExpiryTest()
        {
            var actual = _provider.Add("Name", "AddTest", true);

            Assert.IsTrue(actual);

            Thread.Sleep(6000);

            var expected = _provider.Get("Name");

            Assert.IsNull(expected);
        }

        [TestMethod]
        public void PutWithExpiryCallback()
        {
            var employees = new List<Employee>
            {
                new Employee
                {
                    Name = "EMployee 1"
                }
            };

            _provider.Put("Employees", employees, TimeSpan.FromSeconds(10), ItemCallBack, "Employees");

            _provider.Remove("Employees");

            Thread.Sleep(2000);

            Assert.IsTrue(_callBackCalled);
        }

        [TestMethod]
        public void PutInRegionWithExpiryCallback()
        {
            var employees = new List<Employee>
            {
                new Employee
                {
                    Name = "Employee 1"
                },
            };

            _provider.Put<object>("Employees", "EmpRegion", employees, TimeSpan.FromSeconds(2), ItemCallBackWithObject, new { region = "EmpRegion", key = "Employees" });

            Thread.Sleep(5000);

            Assert.IsTrue(_callBackCalled);
        }

        private void ItemCallBack(string obj)
        {
            _callBackCalled = true;
        }

        private void ItemCallBackWithObject(object payload)
        {
            _callBackCalled = true;
        }
    }

    [Serializable]
    public class Employee
    {
        public string Name { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null)
                throw new NullReferenceException();

            var employee = obj as Employee;
            return this.Name == employee.Name;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
