﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CacheProviderTest.aspx.cs"
    Inherits="CacheProviderTest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Button ID="ButtonAppFabricCacheSimpleTest" runat="server" Text="Test AppFabric Cache (Simple)"
            OnClick="ButtonAppFabricCacheSimpleTest_Click" />
        <asp:Button ID="ButtonAppFabricCacheWithSQLDependency" runat="server" Text="Test AppFabric Cache With SQLDependency"
            OnClick="ButtonAppFabricCacheWithSQLDependency_Click" />
        <asp:Label ID="Label1" runat="server" Text="Info: " Font-Bold="true"></asp:Label>
        <br />
        <br />
        <asp:GridView ID="GridView1" runat="server">
        </asp:GridView>
    </div>
    </form>
</body>
</html>
