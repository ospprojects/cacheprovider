namespace CacheProvider
{
    using Microsoft.Practices.Composite.Presentation.Events;

    public class CacheProviderErrorEvent : CompositePresentationEvent<CacheProviderErrorEventArgs>
    {
    }
}
