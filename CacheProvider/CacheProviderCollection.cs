﻿namespace CacheProvider
{
    #region N A M E S P A C E   I M P O R T S

    using System;
    using System.Configuration.Provider;

    #endregion

    public class CacheProviderCollection : ProviderCollection
    {
        public new CacheProvider this[string name]
        {
            get
            {
                return (CacheProvider)base[name];
            }
        }

        public override void Add(ProviderBase provider)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");

            if (!(provider is CacheProvider))
                throw new ArgumentException("Invalid provider type", "CacheProvider");

            base.Add(provider);
        }
    }    
}
