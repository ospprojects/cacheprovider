namespace CacheProvider
{
    #region N A M E S P A C E   I M P O R T S

    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using CacheProviderEntities;

    #endregion

    [Serializable]
    public class CacheDependencyTracker
    {
        public string Key { get; set; }

        //public Object Value { get; set; }

        public DependencyInfo DependencyData { get; set; }

        //public T DependencyInstance { get; set; }

        //RD starts 6th-Dec-2010
        //public List<T> DependencyInstances { get; set; }

        //RD Ends 6th-Dec-2010
        public string DependencyInstanceID { get; set; }

        public string NamedCache { get; set; }

        public string RegionName { get; set; }

        public bool AreDependenciesCached { get; set; }

        public TimeSpan DelayInDependencyNotificationProcessing { get; set; }

        public Object CacheItemDataVersion { get; set; }

        public CallbackManager CallbackManager
        {
            get
            {
                if (_callbackManager == null)
                    _callbackManager = new CallbackManager();
                return _callbackManager;
            }
        }

        [NonSerialized]
        private CallbackManager _callbackManager;
    }
}