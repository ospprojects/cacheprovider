﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CacheProvider
{
    [Serializable]
    public class CacheClientInfo
    {
        public string ApplicationName { get; set; }
        public string ClientIdentifier { get; set; }
        public bool IsClientAlive { get; set; }
    }
}
