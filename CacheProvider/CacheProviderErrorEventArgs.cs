namespace CacheProvider
{
    using System;

    public class CacheProviderErrorEventArgs : EventArgs
    {
        public object Sender { get; set; }
        public string CacheKey { get; set; }
        public string ErrorMessage { get; set; }
        public object State { get; set; }
    }
}
