namespace CacheProvider
{
    using System.Data.SqlClient;
    using System.Threading;
    using CacheProviderUtilities;

    class QueryNotificationState
    {
        public Timer Sender { get; set; }

        public string Id { get; set; }
    }

    //class SqlNotificationState
    //{
    //    public Timer Sender { get; set; }

    //    public SqlDependency DependencyInstance { get; set; }

    //    public SqlNotificationEventArgs NotificationEventArgs { get; set; }
    //}

    //class OracleNotificationState
    //{
    //    public Timer Sender { get; set; }

    //    public OracleDependency DependencyInstance { get; set; }

    //    public OracleNotificationEventArgs NotificationEventArgs { get; set; }
    //}
}
