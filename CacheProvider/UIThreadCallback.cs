﻿using System;

namespace CacheProvider
{
    using System.Threading;

    public class UIThreadCallback<TPayload>
    {
        private static AsyncCallback SyncContextCallback(AsyncCallback callback)
        {
            var sc = SynchronizationContext.Current;

            if (sc == null) return callback;

            return asyncResult => sc.Post(result => callback((IAsyncResult)result), asyncResult);
        }

        public void InvokeAction(Action<TPayload> action, TPayload argument)
        {
            SynchronizationContext.Current.Post(x => action((TPayload)x), argument);
        }
    }
}
