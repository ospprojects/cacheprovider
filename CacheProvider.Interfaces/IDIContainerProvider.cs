﻿namespace CacheProviderInterfaces
{
    #region N A M E S P A C E   I M P O R T S

    using System;
    using System.Collections;

    #endregion

    public interface IDependencyInjectionContainer
    {
        T Resolve<T>();
        T Resolve<T>(IDictionary arguments);
        T Resolve<T>(object argumentsAsAnonymousType);
        T Resolve<T>(string key);
        object Resolve(Type service);
        T Resolve<T>(string key, IDictionary arguments);
        object Resolve(string key, IDictionary arguments);
        object Resolve(Type service, IDictionary arguments);
        object Resolve(string key, Type service, IDictionary arguments);
        T Resolve<T>(string key, object argumentsAsAnonymousType);
        void RegisterType(Type type, object instance);
        void InitializeWindsorContainer(string configurationPath);
    }
}
