namespace CacheProviderInterfaces
{
    #region N A M E S P A C E   I M P O R T S

    using System;
    using CacheProviderEntities;
    using Microsoft.Practices.Composite.Presentation.Events;

    #endregion

    public interface IDependencyMonitoringManager
    {
        /// <summary>
        /// Sets up monitoring for Dependency Management.
        /// </summary>
        /// <param name="dbConnectionString">DB connection String for which to monitor dependencies</param>
        /// <returns>Returns <see cref="CacheDependencyStatus"/></returns>
        CacheDependencyStatus StartMonitoring(string dbConnectionString);

        /// <summary>
        /// Sets up monitoring for SqlDependency Changes.
        /// </summary>
        /// <param name="dbConnectionString">SQL Server DB connection String for which to stop monitoring dependencies</param>
        /// <returns>Returns <see cref="CacheDependencyStatus"/></returns>
        CacheDependencyStatus StopMonitoring(string dbConnectionString);

        /// <summary>
        /// Registers SqlDependencies for monitoring changes. Has provision for
        /// automatic cache updation (when dependencies change) through callback registration.
        /// </summary>
        /// <typeparam name="T">Type of Payload with which the callback should be invoked.</typeparam>
        /// <param name="key">Cache key</param>
        /// <param name="namedCache">Name of the Named cache (if any & supported)</param>
        /// <param name="regionName">If Region is supported, Name of the Region where the key\value will remain</param>
        /// <param name="dependencyInfo">An instance of <see cref="DependencyInfo" with <see cref="DependencyInfo.DependencyTypes.SQLDependency"/>/> and other suitable properties set.</param>
        /// <param name="encacheDependencies">Whether to store the dependencies in the Cache.  Useful for Distributed cache providers to provide dependency management fallback.</param>
        /// <param name="cacheUpdationCallback">The callback delegate to invoke in case dependencies change.</param>
        /// <param name="callbackState">Payload instance with which the callback should be invoked.</param>
        /// <param name="threadOption">Thread on which the callback should be invoked. See <see cref="ThreadOption"/> for details</param>
        /// <param name="keepSubscriberReferenceAlive">Should there be a strong or weak reference to the callback.</param>
        /// <param name="delayForProcessingDependencyChange">Timespan to signify delay to be introduced while processing a Dependency Change notification.  If it used
        /// so that the win Service does not process before the client(if it is alive) because the client can invoke automatic update callbacks.</param>
        /// <param name="cacheItemVersionData"></param>
        void AddSqlDependencies<T>(string key,
                                   string namedCache,
                                   string regionName,
                                   DependencyInfo dependencyInfo,
                                   bool encacheDependencies,
                                   Action<T> cacheUpdationCallback,
                                   T callbackState,
                                   ThreadOption threadOption,
                                   bool keepSubscriberReferenceAlive,
                                   TimeSpan delayForProcessingDependencyChange,
                                   Object cacheItemVersionData
                                   );

        /// <summary>
        /// Unregisters an already registered SqlDependency from monitoring changes.
        /// </summary>
        /// <param name="key">Cache Key with which Dependency was registered.</param>
        /// <param name="cacheName">Named Cache with which Dependency was registered, else null</param>
        /// <param name="regionName">Name of the region with which dependency was registered, else null</param>
        /// <param name="serializableCacheItemVersion"></param>
        /// <returns>True if the dependency exists and is unregistered else false</returns>
        bool RemoveSqlDependency(string key, string cacheName, string regionName, object serializableCacheItemVersion);

        /// <summary>
        /// Can be used to block other calls if the system is processing a notification callback so that other calls
        /// work on the latest cache state.
        /// This may result in blocking the current call if the system is processing a Data Change Notification callback.
        /// </summary>
        //void WaitForNotificationProcessingToComplete(string key, string region, string namedCache);


       void UpdateDataChangeProcessingDelay(string key, string cacheName, string regionName, TimeSpan newDelay);

       /// <summary>
       /// Can be used to clear all DB Dependencies managed by this instance.
       /// </summary>
       void RemoveAllDependencyTrackers();
    }
}