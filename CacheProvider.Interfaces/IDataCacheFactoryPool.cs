namespace CacheProviderInterfaces
{
    #region N A M E S P A C E   I M P O R T S

    using System;
    using Microsoft.ApplicationServer.Caching;

    #endregion
    public interface IDataCacheFactoryPool
    {
        Int32 PoolSize { get; }
        DataCacheFactory GetDataCacheFactory();
        void ClearPool();
    }
}