namespace CacheProviderInterfaces
{
    #region N A M E S P A C E   I M P O R T S

    using System.Collections.Generic;
    using System.Diagnostics;

    #endregion

    public interface ILogWriter
    {
        bool IsLoggingEnabled { get; }

        void Write(object message);
        void Write(object message, string category);
        void Write(object message, string category, int priority);
        void Write(object message, string category, int priority, int eventId);
        void Write(object message, TraceEventType severity);
        void Write(object message, string category, int priority, int eventId, TraceEventType severity);

        void Write(object message, string category, int priority, int eventId, TraceEventType severity,
                   string title);

        void Write(object message, IDictionary<string, object> properties);
        void Write(object message, string category, IDictionary<string, object> properties);
        void Write(object message, string category, int priority, IDictionary<string, object> properties);

        void Write(object message, string category, int priority, int eventId, TraceEventType severity,
                   string title, IDictionary<string, object> properties);

        void Write(object message, IEnumerable<string> categories);
        void Write(object message, IEnumerable<string> categories, int priority);
        void Write(object message, IEnumerable<string> categories, int priority, int eventId);

        void Write(object message, IEnumerable<string> categories, int priority, int eventId,
                   TraceEventType severity);

        void Write(object message, IEnumerable<string> categories, int priority, int eventId,
                   TraceEventType severity, string title);

        void Write(object message, IEnumerable<string> categories, IDictionary<string, object> properties);

        void Write(object message, IEnumerable<string> categories, int priority,
                   IDictionary<string, object> properties);

        void Write(object message, IEnumerable<string> categories, int priority, int eventId,
                          TraceEventType severity, string title, IDictionary<string, object> properties);
    }
}