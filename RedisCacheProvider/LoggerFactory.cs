﻿using CacheManager.Core.Logging;
using CacheProviderInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CacheProvider.Redis
{
    public class LoggerFactory : ILoggerFactory
    {
        private ILogWriter _logWriter;

        public LoggerFactory(ILogWriter logWriter)
        {
            _logWriter = logWriter;
        }

        public ILogger CreateLogger(string categoryName)
        {
            throw new NotImplementedException();
        }

        public ILogger CreateLogger<T>(T instance)
        {
            throw new NotImplementedException();
        }
    }

    public class Logger : ILogger
    {
        private ILogWriter _logWriter;
        private string _categoryName;

        public Logger(ILogWriter logWriter) :
                this(logWriter, string.Empty)
        {

        }

        public Logger(ILogWriter logWriter, string categoryName)
        {
            _logWriter = logWriter;
            _categoryName = categoryName;
        }

        public IDisposable BeginScope(object state) => null;

        public bool IsEnabled(LogLevel logLevel) => true;

        public void Log(LogLevel logLevel, int eventId, object message, Exception exception)
        {
            if (_logWriter.IsLoggingEnabled)
            {
            }
        }
    }
}
