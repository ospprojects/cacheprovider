﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CacheProviderEntities;
using CacheManager.Core;
using System.Collections.Specialized;
using Microsoft.Practices.Composite.Presentation.Events;
using System.Diagnostics;
using CacheManager.Core.Internal;
using CacheManager.Redis;
using StackExchange.Redis;
using System.Collections.Concurrent;

namespace CacheProvider.Redis
{
    public class RedisCacheProvider : CacheProvider
    {
        private string _name = "CacheProvider.RedisCacheProvider";

        private string _description = "Cache Provider using Redis using CacheManager.StackExchange.Redis";

        private const string _redisConfig = "RedisConfiguration";

        private ConcurrentDictionary<string, CallbackManager> _redisNotifications = new ConcurrentDictionary<string, CallbackManager>();

        private long _defaultExpirationTimeInMS;

        private string _host;

        private int _port;

        private string _password;

        private bool _withSsl;

        private int _databaseIndex;

        private int? _connectionTimeout;

        private ICacheManager<object> _cacheManager;

        private ConnectionMultiplexer _connection;

        public ISubscriber Subscriber => _connection.GetSubscriber();

        public override string Name
        {
            get
            {
                return _name;
            }
        }

        public override string Description
        {
            get
            {
                return _description;
            }
        }

        public override string CacheName
        {
            get
            {
                return string.Empty;
            }
        }

        public override short DataCacheFactoryPoolSize
        {
            get
            {
                return -1;
            }
        }

        public override long DefaultExpirationTimeInMilliSeconds
        {
            get
            {
                return _defaultExpirationTimeInMS;
            }

            set
            {
                _defaultExpirationTimeInMS = value;
            }
        }

        public override bool EagerPoolingEnabled
        {
            get
            {
                return false;
            }
        }

        public override bool IsCacheItemVersioningSupported
        {
            get
            {
                return true;
            }
        }

        public override bool IsCacheTagSupported
        {
            get
            {
                return false;
            }
        }

        public override bool IsConcurrencySupported
        {
            get
            {
                return true;
            }
        }

        public override bool IsNamedCacheSupported
        {
            get
            {
                return false;
            }
        }

        public override bool IsRegionSupported
        {
            get
            {
                return true;
            }
        }

        public override void Initialize(string name, NameValueCollection config)
        {
            if (null == config)
            {
                throw (new ArgumentNullException("config"));
            }

            if (string.IsNullOrEmpty(name))
            {
                name = this._name;
            }
            else
            {
                this._name = name;
            }

            if (string.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", _description);
            }
            else
            {
                this._description = config["description"];
            }

            base.Initialize(name, config);

            DefaultExpirationTimeInMilliSeconds =
                        Convert.ToInt64(config["defaultExpirationTimeInMS"] ?? "60000");

            InitializeSpecificConfig(config);

            InitializeRedisServer();
            RegisterRedisCommandNotification();

            UpdateDependencyTrackerCollection();
        }

        public override bool Add(string key, object value)
        {
            return AddImpl(key, value, TimeSpan.Zero, string.Empty);
        }

        public override bool Add(string key, object value, long ttlInMilliSeconds)
        {
            return AddImpl(key, value, TimeSpan.FromMilliseconds(ttlInMilliSeconds), string.Empty);
        }

        public override bool Add(string key, object value, bool UseDefaultExpiration)
        {
            return AddImpl(key, value, TimeSpan.FromMilliseconds(DefaultExpirationTimeInMilliSeconds), string.Empty);
        }

        public override bool Add(string key, object value, TimeSpan timeout)
        {
            return AddImpl(key, value, timeout, string.Empty);
        }

        public override IDictionary<string, object> Get(params string[] keys)
        {
            IDictionary<string, object> result = null;
            //var _manager = GetManager<object>();

            foreach (var key in keys)
            {
                if (result == null)
                {
                    result = new Dictionary<string, object>();
                }
                result.Add(key, _cacheManager.Get(key));
            }
            return result;
        }

        public override object Get(string key)
        {
            return Get<object>(key);
        }

        public override T Get<T>(string key)
        {
            return Get<T>(key, string.Empty);
        }

        public override T Get<T>(string key, string regionName)
        {
            //var manager = GetManager<T>();

            if (string.IsNullOrEmpty(regionName))
            {
                return _cacheManager.Get<T>(key);
            }

            return _cacheManager.Get<T>(key, regionName);
        }

        public override bool Put(string key, object value)
        {
            //var manager = GetManager<object>();

            _cacheManager.Put(key, value);

            return true;
        }

        public override bool Put(string key, object value, DependencyInfo dependencyInfo)
        {
            return Put(key, value, dependencyInfo, TimeSpan.Zero);
        }

        public override bool Put(string key, object value, TimeSpan timeout)
        {
            return Put(key, string.Empty, value, timeout);
        }

        public override bool PutImpl<T>(string key, string regionName, object value, TimeSpan timeout, Action<T> callback, T cacheState)
        {
            if (callback == null)
            {
                throw new ArgumentNullException("[callback] argument should be null. consider using other Put overload method.");
            }

            if (cacheState == null)
            {
                throw new ArgumentNullException("[cacheState] argument should be null");
            }

            var result = Put(key, regionName, value, timeout);

            if (result)
            {
                var itemKey = !string.IsNullOrEmpty(regionName) ? regionName + ":" + key : key;
                _redisNotifications.AddOrUpdate(itemKey, k =>
                {
                    var callbackManager = new CallbackManager();
                    callbackManager.RegisterCallback(callback, cacheState, ThreadOption.UIThread, true);
                    return callbackManager;

                }, (k, existingManager) =>
                {
                    existingManager.RegisterCallback(callback, cacheState, ThreadOption.UIThread, true);
                    return existingManager;
                });
            }

            return result;
        }

        public override bool Put(string key, object value, bool useDefaultTimeout)
        {
            if (!useDefaultTimeout)
                throw new InvalidOperationException("parameter [useDefaultTimeout] must be true for this Put() Overload");

            var timeout = TimeSpan.FromMilliseconds(this.DefaultExpirationTimeInMilliSeconds);
            return Put(key, value, timeout);
        }

        public override bool Put(string key, object value, DependencyInfo dependencyInfo, TimeSpan timeout)
        {
            return PutWithDependency<object>(key, value, string.Empty, dependencyInfo, timeout, null, null, ThreadOption.UIThread, false);
        }

        public override bool Remove(string key)
        {
            return RemoveImpl(key, string.Empty);
        }

        public override void RemoveAll()
        {
            throw new NotSupportedException("This functionality is not supported in RedisCacheProvider as this action will leads to complete flushing of Redis DB");
        }

        protected override bool RemoveImpl(string key, string regionName)
        {
            bool result = false;

            if (!string.IsNullOrEmpty(regionName))
            {
                _cacheManager.Remove(key + "_CacheVersion", regionName);
                result = _cacheManager.Remove(key, regionName);
            }
            else
            {
                _cacheManager.Remove(key + "_CacheVersion");
                result = _cacheManager.Remove(key);
            }

            return result;
        }

        protected override bool AddImpl(string key, object value, string regionName)
        {
            return AddImpl(key, value, TimeSpan.Zero, regionName);
        }

        protected override bool AddImpl(string key, object value, TimeSpan timeout, string regionName)
        {
            bool result = false;

            CacheItem<object> cacheItem;

            if (!string.IsNullOrEmpty(regionName))
            {
                cacheItem = new CacheItem<object>(key, regionName, value);
            }
            else
            {
                cacheItem = new CacheItem<object>(key, value);
            }

            if (timeout != TimeSpan.Zero)
            {
                cacheItem = cacheItem.WithExpiration(ExpirationMode.Absolute, timeout);
            }

            result = _cacheManager.Add(cacheItem);

            WriteLog(key);

            return result;
        }

        protected override bool PutImpl<T>(string key, object value, string regionName, TimeSpan timeout, IEnumerable<string> cacheTags,
                    DependencyInfo dependencyInfo, Action<T> cacheUpdationCallback, T callbackState, ThreadOption threadOption, bool keepSubscriberReferenceAlive)
        {
            return PutWithDependency(key, value, regionName, dependencyInfo, timeout, cacheUpdationCallback, callbackState, threadOption, keepSubscriberReferenceAlive);
        }

        protected override bool PutImpl<T>(string key, object value, string regionName, bool useDefaultTimeout, IEnumerable<string> cacheTags, DependencyInfo dependencyInfo,
                            Action<T> cacheUpdationCallback, T callbackState, ThreadOption threadOption, bool keepSubscriberReferenceAlive)
        {
            if (!useDefaultTimeout)
                throw new InvalidOperationException("parameter [useDefaultTimeout] must be true for this Put() Overload");

            var defaultTimeout = TimeSpan.FromMilliseconds(this.DefaultExpirationTimeInMilliSeconds);
            return PutImpl(key, value, regionName, defaultTimeout, cacheTags, dependencyInfo, cacheUpdationCallback, callbackState, threadOption, keepSubscriberReferenceAlive);
        }

        /// <summary>
        ///     Method used to compare the old and new cache version
        /// </summary>
        /// <param name="earlierVersion">CacheVersion stored in the cache instance</param>
        /// <param name="key">Unique key to identify the element in the cache</param>
        /// <param name="regionName">Region in Cache Instance</param>
        /// <returns></returns>
        protected override bool IsCacheVersionSameImpl(object earlierVersion, string key, string regionName)
        {
            if (earlierVersion == null)
                return false;

            var oldCacheVersion = Convert.ToDateTime(earlierVersion);
            var newCacheVersion = _cacheManager.Get<DateTime>(key + "_CacheVersion", regionName);

            return oldCacheVersion == newCacheVersion;
        }

        /// <summary>
        ///     Method used to clear all the cache items under a region
        /// </summary>
        /// <param name="regionName"></param>
        /// <returns></returns>
        protected override bool ClearRegionImpl(string regionName)
        {
            _cacheManager.ClearRegion(regionName);
            return true;
        }

        /// <summary>
        ///     Method used to remove all the cache items under a region
        /// </summary>
        /// <param name="regionName"></param>
        /// <returns></returns>
        protected override bool RemoveRegionImpl(string regionName)
        {
            return ClearRegionImpl(regionName);
        }

        /// <summary>
        ///     
        /// </summary>
        protected override void UpdateDependencyTrackerCollection()
        {
            var clientInfos = _cacheManager.Get<List<CacheClientInfo>>(DependencyTrackerKey);
            var isClientExists = false;

            if (clientInfos == null)
            {
                clientInfos = new List<CacheClientInfo>();
            }
            else
            {
                var existingClient = (from clientInfo in clientInfos
                                      where clientInfo.ClientIdentifier.Equals(this.CacheClientNodeIdentifier)
                                      select clientInfo).SingleOrDefault();

                if (existingClient != null)
                {
                    existingClient.IsClientAlive = true;
                    isClientExists = true;
                }
            }

            if (!isClientExists)
            {
                clientInfos.Add(new CacheClientInfo
                {
                    ApplicationName = ApplicationName,
                    ClientIdentifier = CacheClientNodeIdentifier,
                    IsClientAlive = true
                });
            }

            _cacheManager.Put(DependencyTrackerKey, clientInfos);
        }

        /// <summary>
        ///     Method used to set the current client info as offline
        ///     and update the cache information.
        /// </summary>
        /// <param name="clientClosing"></param>
        protected override void NotifyCacheServerImpl(bool clientClosing)
        {
            var clientInfos = _cacheManager.Get<List<CacheClientInfo>>(DependencyTrackerKey);

            if (clientInfos == null) return;

            var currentClientInfo = (from clientInfo in clientInfos
                                     where clientInfo.ClientIdentifier.Equals(CacheClientNodeIdentifier)
                                     select clientInfo).SingleOrDefault();

            if (currentClientInfo == null)
                return;

            currentClientInfo.IsClientAlive = !clientClosing;

            _cacheManager.Put(DependencyTrackerKey, clientInfos);
        }

        /// <summary>
        ///     
        /// </summary>
        /// <param name="config"></param>
        private void InitializeSpecificConfig(NameValueCollection config)
        {
            if (!string.IsNullOrEmpty(config["host"]))
            {
                _host = config["host"];
            }
            else
            {
                _host = "localhost";
            }

            if (!string.IsNullOrEmpty(config["port"]))
            {
                _port = Convert.ToInt32(config["port"]);
            }
            else
            {
                _port = 6379;
            }

            if (!string.IsNullOrEmpty(config["password"]))
            {
                _password = config["password"];
            }

            if (!string.IsNullOrEmpty(config["withssl"]))
            {
                _withSsl = Convert.ToBoolean(config["withssl"]);
            }

            if (!string.IsNullOrEmpty(config["databases"]))
            {
                _databaseIndex = Convert.ToInt32(config["databases"]);
            }

            if (!string.IsNullOrEmpty(config["timeout"]))
            {
                _connectionTimeout = Convert.ToInt32(config["timeout"]);
            }
        }

        private bool Put(string key, string regionName, object value, TimeSpan timeout)
        {
            if (timeout == TimeSpan.Zero)
            {
                throw new ArgumentException("Timeout should be greater than zero for this method call");
            }

            CacheItem<object> cacheItem;

            if (!string.IsNullOrEmpty(regionName))
                cacheItem = new CacheItem<object>(key, regionName, value, ExpirationMode.Absolute, timeout);
            else
                cacheItem = new CacheItem<object>(key, value, ExpirationMode.Absolute, timeout);

            _cacheManager.Put(cacheItem);

            return true;
        }

        /// <summary>
        ///     
        /// </summary>
        /// <param name="message"></param>
        private void LogException(string message)
        {
            LogWriter.Write(message, LoggingCategory, 1, (int)LoggingEvent.Error, TraceEventType.Critical, "RedisCache");
        }

        /// <summary>
        ///     
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="regionName"></param>
        /// <param name="dependencyInfo"></param>
        /// <param name="timeout"></param>
        /// <param name="cacheUpdationCallback"></param>
        /// <param name="callbackState"></param>
        /// <param name="threadOption"></param>
        /// <param name="keepSubscriberReferenceAlive"></param>
        /// <returns></returns>
        private bool PutWithDependency<T>(string key, object value, string regionName, DependencyInfo dependencyInfo, TimeSpan timeout,
                                          Action<T> cacheUpdationCallback, T callbackState, ThreadOption threadOption,
                                          bool keepSubscriberReferenceAlive)
        {
            var result = false;
            if (dependencyInfo != null)
            {
                switch (dependencyInfo.CacheDependencyType)
                {
                    case CacheDependencyTypes.SQLDependency:
                        if (string.IsNullOrEmpty(dependencyInfo.DBConnectionString)
                            || (dependencyInfo.IsSQLSelectQueryBased && dependencyInfo.SelectQueries.Count != 0)
                            || (dependencyInfo.IsSQLTableBased && dependencyInfo.SQLTables.Count != 0)
                            || (dependencyInfo.ISDbCommandBased && dependencyInfo.DbCommands.Count > 0))
                        {
                            ProcessPutWithSqlDependency(key, value, regionName, dependencyInfo, timeout, cacheUpdationCallback, callbackState, threadOption, keepSubscriberReferenceAlive);
                            result = true;
                        }
                        else
                        {
                            throw new ArgumentException("Properties [DBConnectionString],[SelectQuery] and  [IsSQLSelectQuery=true] must be set for CacheDependencyType [SQLDependency]");
                        }
                        break;
                    default:
                        throw new NotSupportedException(dependencyInfo.CacheDependencyType.ToString() + " is not supported");
                }
            }

            return result;
        }

        /// <summary>
        ///     
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="regionName"></param>
        /// <param name="dependencyInfo"></param>
        /// <param name="timeout"></param>
        /// <param name="cacheUpdationCallback"></param>
        /// <param name="callbackState"></param>
        /// <param name="threadOption"></param>
        /// <param name="keepSubscriberReferenceAlive"></param>
        private void ProcessPutWithSqlDependency<T>(string key, object value, string regionName, DependencyInfo dependencyInfo, TimeSpan timeout, Action<T> cacheUpdationCallback, T callbackState, ThreadOption threadOption, bool keepSubscriberReferenceAlive)
        {
            var status = _monitoringManager.StartMonitoring(dependencyInfo.DBConnectionString);

            if (status != CacheDependencyStatus.MonitoringStarted
                && status != CacheDependencyStatus.AlreadyMonitoring)
            {
                var cacheProviderErrorEventArgs = new CacheProviderErrorEventArgs()
                {
                    Sender = this,
                    CacheKey = key,
                    ErrorMessage = "SQL Monitoring could not be started"
                };
                EventAggregatorInstance.GetEvent<CacheProviderErrorEvent>().Publish(cacheProviderErrorEventArgs);
                return;
            }

            if (timeout == TimeSpan.Zero)
            {
                if (!string.IsNullOrEmpty(regionName))
                    _cacheManager.Put(key, value, regionName);
                else
                    _cacheManager.Put(key, value);
            }
            else
            {
                CacheItem<object> cacheItem;

                if (!string.IsNullOrEmpty(regionName))
                    cacheItem = new CacheItem<object>(key, regionName, value, ExpirationMode.Absolute, timeout);
                else
                    cacheItem = new CacheItem<object>(key, value, ExpirationMode.Absolute, timeout);
                _cacheManager.Put(cacheItem);
            }

            try
            {
                var cacheVersion = GetCacheVersion(key, regionName);
                //Save the cache version as createdUTC date
                _cacheManager.Put(key + "_CacheVersion", cacheVersion.CreatedUtc, regionName);
                _monitoringManager.AddSqlDependencies(key, string.Empty, regionName, dependencyInfo, true, cacheUpdationCallback, callbackState, threadOption, keepSubscriberReferenceAlive, TimeSpan.Zero, cacheVersion.CreatedUtc);
                WriteLog(key);
            }
            catch (Exception)//In case of exception, remove the cache entry.
            {
                this.Remove(key);
                throw;
            }
        }

        /// <summary>
        ///     
        /// </summary>
        /// <param name="key"></param>
        /// <param name="regionName"></param>
        /// <returns></returns>
        private CacheItem<object> GetCacheVersion(string key, string regionName)
        {
            if (!string.IsNullOrEmpty(regionName))
                return _cacheManager.GetCacheItem(key, regionName);

            return _cacheManager.GetCacheItem(key);
        }

        /// <summary>
        ///     
        /// </summary>
        /// <param name="key"></param>
        private void WriteLog(string key)
        {
            var message = string.Format("Cache with key {0} added", key);
            // LogWriter.Write(message, string.Empty, 1, (int)LoggingEvent.CachePut, TraceEventType.Information, key);
            var properties = new Dictionary<string, object>();
            properties.Add("priority", 1);
            properties.Add("eventid", (int)LoggingEvent.CachePut);
            properties.Add("severity", TraceEventType.Information);
            properties.Add("title", key);
            LogWriter.Write(message, properties);
        }

        /// <summary>
        ///     
        /// </summary>
        private void RegisterRedisCommandNotification()
        {
            var subscriber = _connection.GetSubscriber();

            subscriber.Subscribe("__keyevent*__:*", (channel, value) =>
            {
                var command = channel.ToString().Split(':')[1];

                if (command != "del" && command != "expired")
                    return;

                var key = value.ToString();
                CallbackManager callbackManager = null;
                if (_redisNotifications.TryRemove(key, out callbackManager))
                {
                    callbackManager.InvokeCallbacks();
                }
            }, CommandFlags.FireAndForget);
        }

        /// <summary>
        ///     
        /// </summary>
        /// <returns></returns>
        private string GetConnectionString()
        {
            var configurationOptions = new ConfigurationOptions()
            {
                AllowAdmin = false,
                ConnectTimeout = _connectionTimeout.HasValue ? _connectionTimeout.Value : 500,
                Password = _password,
                Ssl = _withSsl,
                //SslHost = configuration.SslHost,
                ConnectRetry = 10,
                AbortOnConnectFail = false,
            };

            configurationOptions.EndPoints.Add(_host, _port);

            return configurationOptions.ToString();
        }

        /// <summary>
        ///     
        /// </summary>
        private void InitializeRedisServer()
        {
            var cacheManagerConfig = ConfigurationBuilder.BuildConfiguration(settings =>
            {
                settings
                //.WithLogging(typeof(LoggerFactory), LogWriter)
                .WithRedisBackplane(_redisConfig)
                .WithRedisConfiguration(_redisConfig, dbConfig =>
                {
                    dbConfig.WithEndpoint(_host, _port)
                            .WithPassword(_password);

                    if (_withSsl)
                    {
                        dbConfig.WithSsl();
                    }

                    if (_databaseIndex > 0)
                    {
                        dbConfig.WithDatabase(_databaseIndex);
                    }

                    if (_connectionTimeout.HasValue)
                    {
                        dbConfig.WithConnectionTimeout(_connectionTimeout.Value);
                    }

                }).WithRedisCacheHandle(_redisConfig, true);
            });

            _cacheManager = CacheFactory.FromConfiguration<object>(cacheManagerConfig);

            _connection = ConnectionMultiplexer.Connect(GetConnectionString());
        }
    }
}
