﻿using System;
using CacheDependencyManagerWindowsService;

namespace WinServiceConsoleApp
{
    public class Program
    {
        static void Main(string[] args)
        {
            var cacheProviderWinService = new CacheDependencyMgrWinSvc();
            cacheProviderWinService.ConsoleStart();

            Console.ReadKey();
        }
    }
}
