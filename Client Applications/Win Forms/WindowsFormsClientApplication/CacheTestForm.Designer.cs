﻿namespace WindowsFormsClientApplication
{
    partial class CacheTestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DataGridViewProducts = new System.Windows.Forms.DataGridView();
            this.ButtonGetProductData = new System.Windows.Forms.Button();
            this.Label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewProducts)).BeginInit();
            this.SuspendLayout();
            // 
            // DataGridViewProducts
            // 
            this.DataGridViewProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewProducts.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.DataGridViewProducts.Location = new System.Drawing.Point(0, 151);
            this.DataGridViewProducts.Margin = new System.Windows.Forms.Padding(25);
            this.DataGridViewProducts.Name = "DataGridViewProducts";
            this.DataGridViewProducts.Size = new System.Drawing.Size(579, 388);
            this.DataGridViewProducts.TabIndex = 0;
            // 
            // ButtonGetProductData
            // 
            this.ButtonGetProductData.Location = new System.Drawing.Point(15, 25);
            this.ButtonGetProductData.Name = "ButtonGetProductData";
            this.ButtonGetProductData.Size = new System.Drawing.Size(111, 23);
            this.ButtonGetProductData.TabIndex = 1;
            this.ButtonGetProductData.Text = "Get Product Details";
            this.ButtonGetProductData.UseVisualStyleBackColor = true;
            this.ButtonGetProductData.Click += new System.EventHandler(this.ButtonGetProductData_Click);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(12, 113);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(40, 13);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "Status:";
            // 
            // CacheTestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 539);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.ButtonGetProductData);
            this.Controls.Add(this.DataGridViewProducts);            
            this.Name = "CacheTestForm";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewProducts)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DataGridViewProducts;
        private System.Windows.Forms.Button ButtonGetProductData;
        private System.Windows.Forms.Label Label1;
    }
}

