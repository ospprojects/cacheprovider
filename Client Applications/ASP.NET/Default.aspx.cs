﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DemoWebsite;

//Note: See the configuration -  A Custom HTTP Module has been configured in the web.config
//to handle HTTP Response Caching.

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        using (var context = new AdventureworksDataContext(GetConnectionString()))
        {
            GridView1.DataSource = GetProductDetails(context);
        }

        Page.DataBind();
    }

    private static List<ProductInfo> GetProductDetails(AdventureworksDataContext context)
    {
        var productInfoQuery = from product in context.Products
                               join cat in context.ProductSubcategories
                               on product.ProductSubcategoryID equals cat.ProductSubcategoryID
                               join sizeUnit in context.UnitMeasures
                               on product.SizeUnitMeasureCode equals sizeUnit.UnitMeasureCode into su
                               from availableSizeUnit in su.DefaultIfEmpty() //signifies LEFT OUTER JOIN
                               join weightUnit in context.UnitMeasures on product.WeightUnitMeasureCode equals weightUnit.UnitMeasureCode into wu
                               from availableWeightUnit in wu.DefaultIfEmpty() //signifies LEFT OUTER JOIN
                               select new
                               {
                                   ProductName = product.Name,
                                   ProductCategory = cat.Name,
                                   Price = product.ListPrice,
                                   Size = product.Size,
                                   SizeUnit = availableSizeUnit.Name,
                                   WeightUnit = availableWeightUnit.Name,
                                   Weight = product.Weight
                               };

        var productsInfo = from productData in productInfoQuery
                           select new ProductInfo
                           {
                               ProductName = productData.ProductName,
                               ProductCategory = productData.ProductCategory,
                               Price = productData.Price,
                               Size = productData.Size,
                               SizeUnit = productData.SizeUnit,
                               WeightUnit = productData.WeightUnit,
                               Weight = productData.Weight
                           };

        List<ProductInfo> products = productsInfo.ToList();
        return products;
    }

    private static string GetConnectionString()
    {
        if (Environment.MachineName.Equals("IN8202039D"))
            return @"Data Source=SRVBLRCAF01\MSBLRTCTD01;Integrated Security=true;" +
              "Initial Catalog=AdventureWorks;";

        return @"Data Source=RAHUL-PC\SQL2008EXPRESS;Integrated Security=true;" +
          "Initial Catalog=AdventureWorks;";
    }
}



public class ProductInfo
{
    public string ProductName { get; set; }

    public string ProductCategory { get; set; }

    public decimal Price { get; set; }

    public string Size { get; set; }

    public decimal? Weight { get; set; }

    public string SizeUnit { get; set; }

    public string WeightUnit { get; set; }
}
