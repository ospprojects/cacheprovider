﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Demo.Entities
{
    [Serializable]
    public class Employee
    {
        public int EmployeeID { get; set; }
        public string Name { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as Employee;
            if (other == null)
                return false;
            if (!obj.GetType().Equals(this))
                return false;
            if (other == this)
                return true;
            if (ReferenceEquals(this, other))
                return true;
            if (this.EmployeeID == other.EmployeeID
                && this.Name.Equals(other.Name))
                return true;
            else
                return false;
        }

        public override int GetHashCode()
        {
            return (
                EmployeeID +
                Name).GetHashCode();
        }
    }
}
