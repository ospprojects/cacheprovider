﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Demo.Entities
{
    [Serializable]
    public class Product
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public int Quantity { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as Product;
            if (other == null)
                return false;
            if (!obj.GetType().Equals(this))
                return false;
            if (other == this)
                return true;
            if (ReferenceEquals(this, other))
                return true;
            if (this.ProductId == other.ProductId
                && this.Name.Equals(other.Name)
                && this.Location.Equals(other.Location)
                && this.Quantity.Equals(other.Quantity))
                return true;
            else
                return false;
        }

        public override int GetHashCode()
        {
            return (ProductId +
                Name +
                Location +
                Quantity).GetHashCode();
        }
    }
}
