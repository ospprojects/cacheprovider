﻿namespace DemoWcfService
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.ServiceModel;

    [ServiceContract]
    public interface ITestWcfService
    {
        [OperationContract]
        List<ProductInfo> GetProductInfo_ExplicitCaching();

        [OperationContract]
        List<ProductInfo> GetProductInfo_CachedThroughBehavior();
    }


    [DataContract]
    public class ProductInfo
    {
        [DataMember]
        public string ProductName { get; set; }

        [DataMember]
        public string ProductCategory { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public string Size { get; set; }

        [DataMember]
        public decimal? Weight { get; set; }

        [DataMember]
        public string SizeUnit { get; set; }

        [DataMember]
        public string WeightUnit { get; set; }
    }
}
