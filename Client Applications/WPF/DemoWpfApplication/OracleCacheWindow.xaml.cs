﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CacheProvider;
using Demo.Entities;
using CacheProviderEntities;
using System.Configuration;
using Microsoft.Practices.Composite.Presentation.Events;
using System.Data;
using Oracle.ManagedDataAccess.Client;

namespace DemoWpfApplication
{
    /// <summary>
    /// Interaction logic for OracleCacheWindow.xaml
    /// </summary>
    public partial class OracleCacheWindow : Window
    {
        public OracleCacheWindow()
        {
            InitializeComponent();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            CacheBroker.NotifyCacheClientClosing();
        }

        private void ButtonGetEmployees_Click(object sender, RoutedEventArgs e)
        {
            const string key = "OracleEmployeesCache";
            var empList = CacheBroker.Get<List<Employee>>(key);

            if (empList == null)
            {
                empList = GetEmployees();

                var dependencyInfo = new DependencyInfo(CacheDependencyTypes.SQLDependency)
                {
                    DBName = "OracleEmployeeDB",
                    DBConnectionString = ConfigurationManager.ConnectionStrings["OracleEmployeeDB"].ToString(),
                    IsSQLTableBased = true
                };

                dependencyInfo.SQLTables.Add("EMPLOYEES");

                CacheBroker.Put(key, empList, string.Empty, TimeSpan.Zero, null, dependencyInfo, RefreshData, key, ThreadOption.UIThread, true);

                Label1.Content = "Done!Read From Database";
            }
            else
            {
                Label1.Content = "Done! Read From Cache";
            }

            ListViewEmployees.ItemsSource = empList;

        }

        private static List<Employee> GetEmployees()
        {
            var employees = new List<Employee>();

            using (var oracleConnection = new OracleConnection(ConfigurationManager.ConnectionStrings["OracleEmployeeDB"].ToString()))
            using (var oracleCommand = new OracleCommand("select EMPLOYEE_ID, FIRST_NAME from Employees where EMPLOYEE_ID <= 110", oracleConnection))
            {
                oracleConnection.Open();
                IDataReader reader = oracleCommand.ExecuteReader();

                int idIndex = reader.GetOrdinal("EMPLOYEE_ID");
                int nameIndex = reader.GetOrdinal("FIRST_NAME");
                while (reader.Read())
                {
                    var employee = new Employee
                    {
                        EmployeeID = reader.GetInt32(idIndex),
                        Name = reader.GetString(nameIndex)
                    };

                    employees.Add(employee);
                }
            }

            return employees;
        }

        private void RefreshData(object state)
        {
            Label1.Content = "Dependency Changed! Updating Grid";

            this.Cursor = Cursors.Wait;

            var key = state as string;

            var empList = GetEmployees();

            var dependencyInfo = new DependencyInfo(CacheDependencyTypes.SQLDependency)
            {
                DBName = "EmployeeDB",
                DBConnectionString = ConfigurationManager.ConnectionStrings["OracleEmployeeDB"].ToString(),
                IsSQLTableBased = true
            };

            dependencyInfo.SQLTables.Add("EMPLOYEES");

            CacheBroker.Put(key, empList, string.Empty, TimeSpan.Zero, null, dependencyInfo, RefreshData, key, ThreadOption.UIThread, true);

            ListViewEmployees.ItemsSource = empList;

            Label1.Content = "Grid Updated at " + DateTime.Now;

            this.Cursor = Cursors.Arrow;
        }
    }
}
