﻿namespace DIContainerProvider
{
    #region N A M E S P A C E   I M P O R T S

    using System;
    using System.Linq;
    using CacheProviderEntities;
    using CacheProviderInterfaces;

    #endregion

    public class DIContainerFactory
    {
        public IDependencyInjectionContainer GetDIContainerProvider(DIContainerType containerType, params object[] containerParams)
        {
            IDependencyInjectionContainer diProviderInstance = null;
            switch (containerType)
            {
                case DIContainerType.WindsorCastle:
                    if (containerParams.Count() == 1 && containerParams[0].GetType() == typeof(String))
                    {
                        string configFilePath = containerParams[0].ToString();
                        diProviderInstance = new WindsorContainerProvider(configFilePath);
                    }
                    break;
                default:
                    diProviderInstance = null;
                    break;

            }
            return diProviderInstance;
        }
    }
}
