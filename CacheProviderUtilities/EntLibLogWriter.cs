namespace CacheProviderUtilities
{
    #region N A M E S P A C E   I M P O R T S

    using System.Diagnostics;
    using CacheProviderInterfaces;
    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
    using Microsoft.Practices.EnterpriseLibrary.Logging;

    #endregion

    public class EntLibLogWriter: ILogWriter
    {
        private readonly LogWriter _logWriter = null;

        public EntLibLogWriter()
        {
            EnterpriseLibraryContainer.CreateDefaultContainer();
            _logWriter = EnterpriseLibraryContainer.Current.GetInstance<LogWriter>();
        }

        public bool IsLoggingEnabled
        {
            get { return _logWriter.IsLoggingEnabled(); }
        }

        public void Write(object message)
        {
            if (IsLoggingEnabled)
                _logWriter.Write(message);
        }

        public void Write(object message, string category)
        {
            if (IsLoggingEnabled)
                _logWriter.Write(message, category);
        }

        public void Write(object message, string category, int priority)
        {
            if (IsLoggingEnabled)
                _logWriter.Write(message, category, priority);
        }

        public void Write(object message, string category, int priority, int eventId)
        {
            if (IsLoggingEnabled)
                _logWriter.Write(message, category, priority, eventId);
        }

        public void Write(object message, string category, int priority, int eventId, System.Diagnostics.TraceEventType severity)
        {
            if (IsLoggingEnabled)
                _logWriter.Write(message, category, priority, eventId, severity);
        }

        public void Write(object message, string category, int priority, int eventId, System.Diagnostics.TraceEventType severity, string title)
        {
            if (IsLoggingEnabled)
                _logWriter.Write(message, category, priority, eventId, severity, title);
        }

        public void Write(object message, System.Collections.Generic.IDictionary<string, object> properties)
        {
            if (IsLoggingEnabled)
                _logWriter.Write(message, properties);
        }

        public void Write(object message, string category, System.Collections.Generic.IDictionary<string, object> properties)
        {
            if (IsLoggingEnabled)
                _logWriter.Write(message, category, properties);
        }

        public void Write(object message, string category, int priority, System.Collections.Generic.IDictionary<string, object> properties)
        {
            if (IsLoggingEnabled)
                _logWriter.Write(message, category, priority, properties);

        }

        public void Write(object message, string category, int priority, int eventId, System.Diagnostics.TraceEventType severity, string title, System.Collections.Generic.IDictionary<string, object> properties)
        {
            if (IsLoggingEnabled)
                _logWriter.Write(message, category, priority, eventId, severity, title, properties);
        }

        public void Write(object message, System.Collections.Generic.IEnumerable<string> categories)
        {
            if (IsLoggingEnabled)
                _logWriter.Write(message, categories);
        }

        public void Write(object message, System.Collections.Generic.IEnumerable<string> categories, int priority)
        {
            if (IsLoggingEnabled)
                _logWriter.Write(message, categories, priority);
        }

        public void Write(object message, System.Collections.Generic.IEnumerable<string> categories, int priority, int eventId)
        {
            if (IsLoggingEnabled)
                _logWriter.Write(message, categories, priority, eventId);
        }

        public void Write(object message, System.Collections.Generic.IEnumerable<string> categories, int priority, int eventId, System.Diagnostics.TraceEventType severity)
        {
            if (IsLoggingEnabled)
                _logWriter.Write(message, categories, priority, eventId, severity);
        }

        public void Write(object message, System.Collections.Generic.IEnumerable<string> categories, int priority, int eventId, System.Diagnostics.TraceEventType severity, string title)
        {
            if (IsLoggingEnabled)
                _logWriter.Write(message, categories, priority, eventId, severity, title);
        }

        public void Write(object message, System.Collections.Generic.IEnumerable<string> categories, System.Collections.Generic.IDictionary<string, object> properties)
        {
            if (IsLoggingEnabled)
                _logWriter.Write(message, categories, properties);
        }

        public void Write(object message, System.Collections.Generic.IEnumerable<string> categories, int priority, System.Collections.Generic.IDictionary<string, object> properties)
        {
            if (IsLoggingEnabled)
                _logWriter.Write(message, categories, priority, properties);
        }

        public void Write(object message, System.Collections.Generic.IEnumerable<string> categories, int priority, int eventId, System.Diagnostics.TraceEventType severity, string title, System.Collections.Generic.IDictionary<string, object> properties)
        {
            if (IsLoggingEnabled)
                _logWriter.Write(message, categories, priority, eventId, severity, title, properties);
        }


        #region ILogWriter Members


        public void Write(object message, TraceEventType severity)
        {
            throw new System.NotImplementedException();
        }

        #endregion
    }
}