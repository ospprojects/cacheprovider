﻿namespace CacheDependencyManagerWindowsService
{
    #region N A M E S P A C E   I M P O R T S

    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading;
    using CacheProvider;
    using CacheProvider.AppFabric;
    using CacheProviderEntities;
    using CacheProviderInterfaces;
    using CacheProviderUtilities;
    using Microsoft.ApplicationServer.Caching;
    using Microsoft.Practices.Composite.Presentation.Events;
    using System.Data.SqlClient;

    #endregion

    public class AppFabricCacheDependencyProcessor : CacheDependencyProcessor
    {
        private readonly List<DataCacheNotificationDescriptor> _dataCacheNotificationDescriptors;

        private readonly AppFabricCacheProvider _appFabricCacheProvider;

        const DataCacheOperations NotificationFilters = DataCacheOperations.AddItem |
                                                        DataCacheOperations.RemoveItem |
                                                        DataCacheOperations.ReplaceItem;


        private DataCacheNotificationDescriptor _dataCacheNotificationDescriptor;

        public AppFabricCacheDependencyProcessor(int dependencyNotificationDelayInSeconds)
                : base(dependencyNotificationDelayInSeconds)
        {
            _dataCacheNotificationDescriptors = new List<DataCacheNotificationDescriptor>();

            _appFabricCacheProvider = _cacheProvider as AppFabricCacheProvider;

            Debug.Assert(_appFabricCacheProvider != null, "Appfabric must be the default provider configured in the configuration file");

        }

        public override bool StartDependencyMonitoring()
        {
            _dataCacheNotificationDescriptor = _appFabricCacheProvider.AddItemLevelCallback(_appFabricCacheProvider.DependencyTrackerKey,
                                                                                                 NotificationFilters,
                                                                                                 ClientNodesNotificationCallback,
                                                                                                 string.Empty);
            this._dataCacheNotificationDescriptors.Add(_dataCacheNotificationDescriptor);

            //this.ProcessDependencies();
            this.ProcessCacheClientNodesCollection();

            const string message = "Cache Dependency Monitor Windows Service Started Monitoring";
            this._logWriter.Write(message, this._appFabricCacheProvider.LoggingCategory, 1, (int)CacheOperation.DependencyMonitorSvc, TraceEventType.Information, "StartDependencyMonitoring()");

            return true;
        }

        public override void StopDependencyMonitoring()
        {
            if (this._dataCacheNotificationDescriptors != null)
            {
                foreach (var dataCacheNotificationDescriptor in _dataCacheNotificationDescriptors)
                {
                    this._appFabricCacheProvider.RemoveCallback(dataCacheNotificationDescriptor);
                }
            }
            //if (this._dataCacheNotificationDescriptor != null)
            //    this._appFabricCacheProvider.RemoveCallback(this._dataCacheNotificationDescriptor);

            var cacheDependencyTrackers = _appFabricCacheProvider.Get(_appFabricCacheProvider.DependencyTrackerKey) as List<CacheDependencyTracker>;

            if (cacheDependencyTrackers != null)
                foreach (var dependencyTracker in cacheDependencyTrackers)
                {
                    this._monitoringManager.RemoveSqlDependency(dependencyTracker.Key, dependencyTracker.NamedCache,
                                                                   dependencyTracker.RegionName, null);
                }

            const string message = "Cache Dependency Monitor Windows Service Stopped";
            this._logWriter.Write(message, this._appFabricCacheProvider.LoggingCategory, 1, (int)CacheOperation.DependencyMonitorSvc, TraceEventType.Information, "StopMonitoring()");
        }

        #region P R I V A T E   M E T H O D S

        private void ClientNodesNotificationCallback(string cacheName, string regionName, string key,
                                           DataCacheItemVersion version, DataCacheOperations cacheOperation,
                                           DataCacheNotificationDescriptor notificationDescriptor)
        {
            this.ProcessCacheClientNodesCollection();
        }

        private void ProcessCacheClientNodesCollection()
        {
            var appFabricCacheClientInfos = this._appFabricCacheProvider.Get<List<AppFabricCacheClientInfo>>(_appFabricCacheProvider.DependencyTrackerKey);
            if (appFabricCacheClientInfos == null)
                return;

            var clientsThatExited = from clientInfo in appFabricCacheClientInfos
                                    where clientInfo.IsClientAlive == false
                                    select clientInfo;

            this.ProcessClosedClients(clientsThatExited);

            foreach (var appFabricCacheClientInfo in appFabricCacheClientInfos.Where(client => client.IsClientAlive))
            {
                var message = string.Format("Cache notification callback added for node {0}", appFabricCacheClientInfo.ClientIdentifier);
                var dataCacheNotificationDescriptor = this._appFabricCacheProvider.AddItemLevelCallback(
                                                                            appFabricCacheClientInfo.ClientIdentifier,
                                                                            NotificationFilters,
                                                                            DependencyTrackerListChangeNotificationCallback,
                                                                            string.Empty);
                this._dataCacheNotificationDescriptors.Add(dataCacheNotificationDescriptor);

                if (this._logWriter != null)
                    this._logWriter.Write(message, this._appFabricCacheProvider.LoggingCategory, 1, (int)CacheOperation.DependencyMonitorSvc,
                                          TraceEventType.Information, "ProcessCacheClientNodesCollection()");


                ThreadPool.QueueUserWorkItem(this.ProcessDependencies, appFabricCacheClientInfo.ClientIdentifier);
            }
        }

        /// <summary>
        /// For AppFabric Cache Clients that have exited, Get its
        /// Dependency Tracker list and update the Delay in processing to immediate
        /// for any data change notification.
        /// </summary>
        /// <param name="closedClients"></param>
        private void ProcessClosedClients(IEnumerable<AppFabricCacheClientInfo> closedClients)
        {
            foreach (var closedClient in closedClients)
            {
                var cacheDependencyTrackers = this._appFabricCacheProvider.Get(closedClient.ClientIdentifier) as List<CacheDependencyTracker>;
                if (cacheDependencyTrackers != null)
                {
                    foreach (var dependencyTracker in cacheDependencyTrackers)
                    {
                        this._monitoringManager.UpdateDataChangeProcessingDelay(dependencyTracker.Key,
                                                                                   dependencyTracker.NamedCache,
                                                                                   dependencyTracker.RegionName,
                                                                                   TimeSpan.Zero);
                    }
                }
            }
        }

        /// <summary>
        /// This callback is invoked by AppFabric if 
        /// the CacheDependencyTracker list changes for a Cache Client Node.
        /// </summary>
        /// <param name="cacheName"></param>
        /// <param name="regionName"></param>
        /// <param name="key"></param>
        /// <param name="version"></param>
        /// <param name="cacheOperation"></param>
        /// <param name="notificationDescriptor"></param>
        private void DependencyTrackerListChangeNotificationCallback(string cacheName, string regionName, string key,
                                                   DataCacheItemVersion version, DataCacheOperations cacheOperation,
                                                   DataCacheNotificationDescriptor notificationDescriptor)
        {
            if (cacheOperation == DataCacheOperations.AddItem
                || cacheOperation == DataCacheOperations.RemoveItem
                || cacheOperation == DataCacheOperations.ReplaceItem)
            {
                ProcessDependencies(key);
            }
        }

        private void ProcessDependencies(object key)
        {
            try
            {
                var cacheDependencyTrackers = _appFabricCacheProvider.Get(key.ToString()) as List<CacheDependencyTracker>;

                if (cacheDependencyTrackers != null)
                    foreach (var dependencyTracker in cacheDependencyTrackers)
                    {
                        var dependencyInfo = dependencyTracker.DependencyData;

                        if (dependencyInfo.CacheDependencyType != CacheDependencyTypes.SQLDependency) continue;

                        if (dependencyInfo.ISDbCommandBased)
                        {
                            CacheDBCommandHelper.RestoreDBCommandParameters(dependencyInfo);
                        }

                        var status = _monitoringManager.StartMonitoring(dependencyInfo.DBConnectionString);

                        if (status != CacheDependencyStatus.MonitoringStarted
                            && status != CacheDependencyStatus.AlreadyMonitoring)
                        {
                            var message = "Could not start SQL Dependency Monitoring In Cache Dependency Monitor Windows Service!";
                            this._logWriter.Write(message, _appFabricCacheProvider.LoggingCategory, 1, (int)CacheOperation.DependencyMonitorSvc, TraceEventType.Critical, "ProcessDependencies()");
                            return;
                        }

                        this._monitoringManager.AddSqlDependencies<Object>(
                                                                        dependencyTracker.Key, dependencyTracker.NamedCache,
                                                                        dependencyTracker.RegionName, dependencyInfo, false, null,
                                                                        null, ThreadOption.PublisherThread, false,
                                                                         TimeSpan.FromSeconds(this._dependencyNotificationDelayInSeconds),
                                                                        dependencyTracker.CacheItemDataVersion);
                    }

                if (this._logWriter != null)
                {
                    var logMessage = string.Format("Added SQL dependency Monitoring for all CacheDependencyTrackers under CacheItemKey:{0} by ProcessDependencies method", key);
                    this._logWriter.Write(logMessage, _appFabricCacheProvider.LoggingCategory, 1, (int)CacheOperation.DependencyMonitorSvc, TraceEventType.Information, "ProcessDependencies()");
                }
            }
            catch (Exception exception)
            {
                if (this._logWriter != null)
                    this._logWriter.Write(exception.Message, _appFabricCacheProvider.LoggingCategory, 1, (int)CacheOperation.DependencyMonitorSvc, TraceEventType.Critical, "ProcessDependencies()");
            }
        }

        #endregion
    }
}
