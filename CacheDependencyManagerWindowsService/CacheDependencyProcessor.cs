﻿namespace CacheDependencyManagerWindowsService
{
    #region N A M E S P A C E   I M P O R T S

    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading;
    using CacheProvider;
    using CacheProvider.AppFabric;
    using CacheProviderEntities;
    using CacheProviderInterfaces;
    using CacheProviderUtilities;
    using Microsoft.ApplicationServer.Caching;
    using Microsoft.Practices.Composite.Presentation.Events;
    using System.Data.SqlClient;

    #endregion

    public abstract class CacheDependencyProcessor
    {
        #region P R I V A T E   A T T R I B U T E S

        const DataCacheOperations NotificationFilters = DataCacheOperations.AddItem |
                                                        DataCacheOperations.RemoveItem |
                                                        DataCacheOperations.ReplaceItem;

        protected readonly IDependencyMonitoringManager _monitoringManager;

        protected readonly ILogWriter _logWriter;

        protected readonly int _dependencyNotificationDelayInSeconds;

        protected readonly CacheProvider _cacheProvider;

        #endregion

        #region C O N S T R U C T O R ( S )

        public CacheDependencyProcessor(int dependencyNotificationDelayInSeconds)
        {
            _dependencyNotificationDelayInSeconds = dependencyNotificationDelayInSeconds;
            _cacheProvider = CacheBroker.Provider;

            _logWriter = new EntLibLogWriter();

            switch (_cacheProvider.Database)
            {
                case Database.SqlServer:
                    this._monitoringManager = new SqlMonitoringManager(_cacheProvider, _cacheProvider.DIContainer);
                    break;
                case Database.Oracle:
                    this._monitoringManager = new OracleMonitoringManager(CacheBroker.Provider, _cacheProvider.DIContainer);
                    break;
            }
        }

        #endregion

        public abstract bool StartDependencyMonitoring();

        public abstract void StopDependencyMonitoring();

        /// <summary>
        /// For Distributed Cache Clients that have exited, Get its
        /// Dependency Tracker list and update the Delay in processing to immediate
        /// for any data change notification.
        /// </summary>
        /// <param name="closedClients"></param>
        protected virtual void ProcessClosedClients(IEnumerable<CacheClientInfo> closedClients)
        {
            foreach (var closedClient in closedClients)
            {
                var cacheDependencyTrackers = this._cacheProvider.Get<List<CacheDependencyTracker>>(closedClient.ClientIdentifier);
                if (cacheDependencyTrackers != null)
                {
                    foreach (var dependencyTracker in cacheDependencyTrackers)
                    {
                        _monitoringManager.UpdateDataChangeProcessingDelay(dependencyTracker.Key,
                                                                                   dependencyTracker.NamedCache,
                                                                                   dependencyTracker.RegionName,
                                                                                   TimeSpan.Zero);
                    }
                }
            }
        }
    }
}
