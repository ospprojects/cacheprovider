﻿using CacheProvider;
using CacheProvider.Redis;
using CacheProviderEntities;
using Microsoft.Practices.Composite.Presentation.Events;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CacheDependencyManagerWindowsService
{
    public class RedisCacheDependencyProcessor : CacheDependencyProcessor
    {
        private readonly RedisCacheProvider _redisCacheProvider;

        private readonly string _redisSubscriptionChannel;
        public RedisCacheDependencyProcessor(int dependencyNotificationDelayInSeconds)
                : base(dependencyNotificationDelayInSeconds)
        {
            _redisCacheProvider = _cacheProvider as RedisCacheProvider;

            _redisSubscriptionChannel = "__keyspace@*__:" + _cacheProvider.DependencyTrackerKey;

            Debug.Assert(_redisCacheProvider != null, "RedisCacheProvider must be the default provider configured in the configuration file");
        }

        public override bool StartDependencyMonitoring()
        {
            _redisCacheProvider.Subscriber.Subscribe(_redisSubscriptionChannel, (channel, value) =>
            {
                if (value.ToString() == "hset")
                {
                    ProcessCacheClientNodesCollection();
                }

            }, CommandFlags.FireAndForget);

            ProcessCacheClientNodesCollection();

            return true;
        }

        public override void StopDependencyMonitoring()
        {
            _redisCacheProvider.Subscriber.UnsubscribeAll();

            var cacheDependencyTrackers = _redisCacheProvider.Get<List<CacheDependencyTracker>>(_redisCacheProvider.DependencyTrackerKey);

            if (cacheDependencyTrackers != null)
                foreach (var dependencyTracker in cacheDependencyTrackers)
                {
                    _monitoringManager.RemoveSqlDependency(dependencyTracker.Key, dependencyTracker.NamedCache,
                                                                   dependencyTracker.RegionName, null);
                }

            const string message = "Cache Dependency Monitor Windows Service Stopped";
            _logWriter.Write(message, _redisCacheProvider.LoggingCategory, 1, (int)CacheOperation.DependencyMonitorSvc, TraceEventType.Information, "StopMonitoring()");
        }

        private void ProcessCacheClientNodesCollection(object state = null)
        {
            var cacheClientInfos = _redisCacheProvider.Get<List<CacheClientInfo>>(_redisCacheProvider.DependencyTrackerKey);
            if (cacheClientInfos == null)
                return;

            var clientsThatExited = from clientInfo in cacheClientInfos
                                    where clientInfo.IsClientAlive == false
                                    select clientInfo;

            ProcessClosedClients(clientsThatExited);

            foreach (var clientInfo in cacheClientInfos.Where(client => client.IsClientAlive))
            {
                var message = string.Format("Cache notification callback added for node {0}", clientInfo.ClientIdentifier);

                _redisCacheProvider.Subscriber.Subscribe("__keyspace@*__:" + clientInfo.ClientIdentifier, (channel, value) =>
                {
                    if (value.ToString() == "hset")
                    {
                        var key = channel.ToString().Split(':')[1];
                        ProcessDependencies(key);
                    }

                }, CommandFlags.FireAndForget);

                if (_logWriter != null)
                    _logWriter.Write(message, _redisCacheProvider.LoggingCategory, 1, (int)CacheOperation.DependencyMonitorSvc,
                                          TraceEventType.Information, "ProcessCacheClientNodesCollection()");


                ThreadPool.QueueUserWorkItem(ProcessDependencies, clientInfo.ClientIdentifier);
            }
        }

        private void ProcessDependencies(object key)
        {
            try
            {
                var cacheDependencyTrackers = _redisCacheProvider.Get<List<CacheDependencyTracker>>(key.ToString());

                if (cacheDependencyTrackers != null)
                    foreach (var dependencyTracker in cacheDependencyTrackers)
                    {
                        var dependencyInfo = dependencyTracker.DependencyData;

                        if (dependencyInfo.CacheDependencyType != CacheDependencyTypes.SQLDependency) continue;

                        if (dependencyInfo.ISDbCommandBased)
                        {
                            CacheDBCommandHelper.RestoreDBCommandParameters(dependencyInfo);
                        }

                        var status = _monitoringManager.StartMonitoring(dependencyInfo.DBConnectionString);

                        if (status != CacheDependencyStatus.MonitoringStarted
                            && status != CacheDependencyStatus.AlreadyMonitoring)
                        {
                            var message = "Could not start SQL Dependency Monitoring In Cache Dependency Monitor Windows Service!";
                            this._logWriter.Write(message, _redisCacheProvider.LoggingCategory, 1, (int)CacheOperation.DependencyMonitorSvc, TraceEventType.Critical, "ProcessDependencies()");
                            return;
                        }

                        _monitoringManager.AddSqlDependencies<Object>(dependencyTracker.Key, dependencyTracker.NamedCache,
                                                                        dependencyTracker.RegionName, dependencyInfo, false, null,
                                                                        null, ThreadOption.PublisherThread, false,
                                                                         TimeSpan.FromSeconds(_dependencyNotificationDelayInSeconds),
                                                                        dependencyTracker.CacheItemDataVersion);
                    }

                if (this._logWriter != null)
                {
                    var logMessage = string.Format("Added SQL dependency Monitoring for all CacheDependencyTrackers under CacheItemKey:{0} by ProcessDependencies method", key);
                    this._logWriter.Write(logMessage, _redisCacheProvider.LoggingCategory, 1, (int)CacheOperation.DependencyMonitorSvc, TraceEventType.Information, "ProcessDependencies()");
                }
            }
            catch (Exception exception)
            {
                if (this._logWriter != null)
                    this._logWriter.Write(exception.Message, _redisCacheProvider.LoggingCategory, 1, (int)CacheOperation.DependencyMonitorSvc, TraceEventType.Critical, "ProcessDependencies()");
            }
        }
    }
}
