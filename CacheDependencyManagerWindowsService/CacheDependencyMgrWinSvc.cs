﻿using System;
using System.Diagnostics;


namespace CacheDependencyManagerWindowsService
{
    using System.Configuration;
    using System.ServiceProcess;
    using CacheProviderInterfaces;
    using CacheProvider;
    using CacheProviderUtilities;
    using CacheProviderEntities;
    

    public partial class CacheDependencyMgrWinSvc : ServiceBase
    {
        private CacheDependencyProcessor _cacheDependencyProcessor;
        private ILogWriter _logWriter;

        public CacheDependencyMgrWinSvc()
        {
            InitializeComponent();
            AppDomain.CurrentDomain.UnhandledException += this.CurrentDomain_UnhandledException;
        }

        protected void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (this._logWriter == null) return;

            var exception = e.ExceptionObject as Exception;
            if (exception != null)
                this._logWriter.Write(exception.Message, ConfigurationManager.AppSettings["ApplicationName"], 1, (int)CacheOperation.DependencyMonitorSvc, TraceEventType.Error, "UnhandledException");
        }

        protected override void OnStart(string[] args)
        {
            this._logWriter = new EntLibLogWriter();
            var dependencyNotificationProcessingDelayInSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["DependencyNotificationProcessingDelayInSeconds"]);
            this._cacheDependencyProcessor = new RedisCacheDependencyProcessor(dependencyNotificationProcessingDelayInSeconds);
            this._cacheDependencyProcessor.StartDependencyMonitoring();
            const string message = "Cache Dependency Monitor Windows Service Started";
            this._logWriter.Write(message, ConfigurationManager.AppSettings["ApplicationName"], 1, (int)CacheOperation.DependencyMonitorSvc, TraceEventType.Information, "OnStart()");
        }

        public void ConsoleStart()
        {
            this.OnStart(null);
        }

        protected override void OnStop()
        {
            this._cacheDependencyProcessor.StartDependencyMonitoring();
        }
    }
}
