﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CacheProviderEntities
{
    public class MonitoringInfo
    {
        public DateTime MonitoringStartTime { get; set; }
        public CacheDependencyStatus Status { get; set; }
    }
}
