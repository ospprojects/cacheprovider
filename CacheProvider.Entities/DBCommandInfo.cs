namespace CacheProviderEntities
{
    #region N A M E S P A C E   I M P O R T S

    using System;
    using System.Collections.Generic;

    #endregion

    [Serializable]
    public class DBCommandInfo
    {
        public string StoredProcedureName { get; set; }

        public List<Tuple<string, string>> Parameters { get; set; }
    }
}