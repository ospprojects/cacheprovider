namespace CacheProviderEntities
{
    #region N A M E S P A C E   I M P O R T S

    using System;
    using System.Collections.Generic;
    using System.Data.Common;

    #endregion

    [Serializable]
    public class DependencyInfo
    {
        public CacheDependencyTypes CacheDependencyType { get { return this._cacheDependencyType; } }

        public List<String> Files
        {
            get
            {
                return this._files;
            }
            set
            {
                if (this._cacheDependencyType != CacheDependencyTypes.File)
                    throw new InvalidOperationException(string.Format("Setting Files property for CacheDependencyType: {0} is invalid", this._cacheDependencyType));
                this._files = value;
            }
        }

        public List<string> CacheKeys { get; set; }

        /// <summary>
        /// Set it to true if <see cref="SelectQueries"/> property is set to atleast one entry.
        /// </summary>
        public bool IsSQLSelectQueryBased { get; set; }

        /// <summary>
        /// Set it to true if <see cref="SQLTables"/> property is set to atleast one entry.
        /// </summary>
        public bool IsSQLTableBased { get; set; }

        public string DBName { get; set; }

        /// <summary>
        /// ConnectionString of the DB for monitoring dependencies
        /// </summary>
        public string DBConnectionString { get; set; }

        /// <summary>
        /// Add one or more supported SQL Queries for monitoring dependency changes.
        /// /// Set the <see cref="IsSQLSelectQueryBased"/> property to true if atleast one entry is added to this.
        /// </summary>
        public List<string> SelectQueries { get; set; }

        /// <summary>
        /// Add one or more SQL Tables for monitoring Dependency changes.
        /// Table Names can be two part to signify [SchemaName].[TableName]
        /// Set the <see cref="IsSQLTableBased"/> property to true if atleast one entry is added to this.
        /// </summary>
        public List<string> SQLTables { get; set; }

        [NonSerialized]
        private List<DbCommand> _dbCommands;


        public List<DbCommand> DbCommands
        {
            get { return this._dbCommands; }
            set
            {
                this._dbCommands = value;
                CacheDBCommandHelper.PersistDBCommandParamters(this);
            }
        }

        public bool ISDbCommandBased { get; set; }

        //DBCommand information list 
        public List<DBCommandInfo> DbCommandInfos
        {
            get
            {
                return this._dbCommandInfos;
            }
            set
            {
                this._dbCommandInfos = value;
            }
        }

        public DependencyInfo(CacheDependencyTypes cacheDependencyType)
        {
            this._cacheDependencyType = cacheDependencyType;
            //RD starts 6th-Dec-2010
            this.SelectQueries = new List<string>();
            this.SQLTables = new List<string>();
            //RD ends 6th-Dec-2010
            this.DbCommandInfos = new List<DBCommandInfo>();
        }

        private readonly CacheDependencyTypes _cacheDependencyType;
        private List<String> _files;
        private List<DBCommandInfo> _dbCommandInfos;
    }
}