﻿namespace CacheProviderEntities
{
    using System;

    // To be removed when we will use C# 4 :)
    [Serializable]
    public class Tuple<U, V>
    {
        public Tuple(U u, V v)
        {
            Item1 = u;
            Item2 = v;
        }

        public Tuple()
        {
        }

        public U Item1 { get; set; }
        public V Item2 { get; set; }
    }
    [Serializable]
    public class Tuple<U, V, W>
    {
        public Tuple()
        {
        }

        public Tuple(U u, V v, W w)
        {
            Item1 = u;
            Item2 = v;
            Item3 = w;
        }

        public U Item1 { get; set; }
        public V Item2 { get; set; }
        public W Item3 { get; set; }
    }
    [Serializable]
    public class Tuple<U, V, W, X>
    {
        public Tuple()
        {
        }

        public Tuple(U u, V v, W w, X x)
        {
            Item1 = u;
            Item2 = v;
            Item3 = w;
            Item4 = x;
        }

        public U Item1 { get; set; }
        public V Item2 { get; set; }
        public W Item3 { get; set; }
        public X Item4 { get; set; }
    }

    // ... etc until 1..8 :p
}
