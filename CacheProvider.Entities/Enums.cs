namespace CacheProviderEntities
{
    #region Cache Provider Enumeration

    public enum CacheDependencyTypes
    {
        File,
        FileNCacheKey,
        SQLDependency
    }

    public enum DIContainerType
    {
        WindsorCastle,
        Unity
    }

    public enum CacheDependencyStatus
    {
        Unknown,
        MonitoringStarted,//This is the only possible value when an entry is there in Dictionary.
        AlreadyMonitoring,
        InvalidConnectionString,
        MonitoringStopped,
        NotRegistered
    }

    public enum LoggingEvent
    {
        CachePut = 1,
        CacheGet = 2,
        CacheRemove = 3,
        CacheNotification = 4,
        Error = 5,
        DependencyMonitorSvc = 6,
        ReferalService = 7,
        ReferalServiceException = 8,
        DataCacheError = 9
    }

    public enum CacheOperation
    {
        Put = 1,
        Get = 2,
        Remove = 3,
        CacheNotification = 4,
        Error = 5,
        DependencyMonitorSvc = 6
    }

    public enum Database
    {
        SqlServer,
        Oracle,
        RavenDB
    }
    
    #endregion
}