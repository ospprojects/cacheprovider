﻿namespace CacheBehaviorExtension
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ServiceModel.Description;
    using CacheProvider;
    using CacheProviderEntities;

    public class CacheServiceBehavior : Attribute, IServiceBehavior
    {

        [Description("Unique Key for Cache.")]
        public string Key { get; set; }

        [Description("The time that the response object will remain in Cache. If not set, behavior depends on configured provider.")]
        public TimeSpan Timeout { get; set; }

        [Description(@"Name of the Region under which the Key\Value will remain in Cache.  Region must pre-exist and should be supported by the configured provider. If not supported, this value has no impact")]
        public string RegionName { get; set; }

        [Description("Tags to annotage objects. It should be supported by the configured provider. If not supported, this value has no impact")]
        public List<string> CacheTags { get; set; }

        //[Description("Dependencies that if change will invalidate the related Cache entry")]
        //public DependencyInfo DependencyDetails { get; set; }

        public CacheDependencyTypes DependencyType { get; set; }

        public string DependencyDbConnectionString { get; set; }

        public string DependencySqlQuery { get; set; }

        public string DependencyDbName { get; set; }


        #region IServiceBehavior Members

        public void AddBindingParameters(ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase, System.Collections.ObjectModel.Collection<ServiceEndpoint> endpoints, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase)
        {
            foreach (var endpoint in serviceDescription.Endpoints)
            {
                foreach (var operation in endpoint.Contract.Operations)
                {
                    operation.Behaviors.Add(
                                                new CachingOperationBehavior()
                                                {
                                                    CacheTags = this.CacheTags,
                                                    Key = this.Key,
                                                    Timeout = this.Timeout,
                                                    RegionName = this.RegionName,
                                                    DependencyDbConnectionString = this.DependencyDbConnectionString,
                                                    DependencyDbName = this.DependencyDbName,
                                                    DependencySqlQuery = this.DependencySqlQuery,
                                                    DependencyType = this.DependencyType
                                                }
                                            );
                }
            }
        }

        public void Validate(ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase)
        {
        }

        #endregion
    }
}
